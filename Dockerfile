FROM openjdk:9-slim

LABEL maintainer="e-COSI <tech@e-cosi.com>"

ENV BASTILLION_VERSION=v3.10.00 \
    BASTILLION_FILENAME=v3.10_00 \
    DOCKERIZE_VERSION=v0.6.1

RUN apt-get update && apt-get -y install wget
RUN   wget --quiet https://github.com/bastillion-io/Bastillion/releases/download/${BASTILLION_VERSION}/bastillion-jetty-${BASTILLION_FILENAME}.tar.gz
RUN    wget --quiet https://github.com/jwilder/dockerize/releases/download/${DOCKERIZE_VERSION}/dockerize-linux-amd64-${DOCKERIZE_VERSION}.tar.gz
RUN    tar xzf bastillion-jetty-${BASTILLION_FILENAME}.tar.gz -C /opt 
RUN    tar xzf dockerize-linux-amd64-${DOCKERIZE_VERSION}.tar.gz -C /usr/local/bin 
RUN    mv /opt/Bastillion-jetty /opt/bastillion 
RUN    rm bastillion-jetty-${BASTILLION_FILENAME}.tar.gz dockerize-linux-amd64-${DOCKERIZE_VERSION}.tar.gz 
RUN    apt-get remove --purge -y wget && apt-get -y autoremove && rm -rf /var/lib/apt/lists/* 
# create db directory for later permission update
RUN        mkdir /opt/bastillion/jetty/bastillion/WEB-INF/classes/keydb 
# remove default config - will be written by dockerize on startup
RUN        rm /opt/bastillion/jetty/bastillion/WEB-INF/classes/BastillionConfig.properties

# persistent data of Bastillion is stored here
VOLUME /opt/bastillion/jetty/bastillion/WEB-INF/classes/keydb

# this is the home of Bastillion
WORKDIR /opt/bastillion

# Bastillion listens on 8443 - HTTPS
EXPOSE 8443

# Bastillion configuration template for dockerize
ADD files/BastillionConfig.properties.tpl /opt

# Configure Jetty
ADD files/jetty-start.ini /opt/bastillion/jetty/start.ini

# Custom Jetty start script
ADD files/startBastillion.sh /opt/bastillion/startBastillion.sh

# correct permission for running as non-root (f.e. on OpenShift)
RUN chmod 755 /opt/bastillion/startBastillion.sh && \
    chgrp -R 0 /opt/bastillion && \
    chmod -R g=u /opt/bastillion

# dont run as root
USER 1001

ENTRYPOINT ["/usr/local/bin/dockerize"]
CMD ["-template", \
     "/opt/BastillionConfig.properties.tpl:/opt/bastillion/jetty/bastillion/WEB-INF/classes/BastillionConfig.properties", \
     "/opt/bastillion/startBastillion.sh"]
